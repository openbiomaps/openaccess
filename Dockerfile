FROM  mdillon/postgis:9.6 AS base

ENV POSTGRES_USER openaccess
ENV POSTGRES_PASSWORD changeMe

FROM base AS test
COPY initdb.d/ /docker-entrypoint-initdb.d/
COPY sql/ /docker-entrypoint-initdb.d/sql/
COPY lists/ /docker-entrypoint-initdb.d/lists/
#RUN chmod 444 -R /docker-entrypoint-initdb.d/*
RUN sed -i '$ d' /usr/local/bin/docker-entrypoint.sh

FROM  base
COPY --from=test /docker-entrypoint-initdb.d/  /docker-entrypoint-initdb.d/

